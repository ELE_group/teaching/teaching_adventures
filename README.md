# TEACHING ADVENTURES

## slides
- version 1.0 of [presentation (for DTU Elektro staff meeting, 2021-08-26) on videos and corona-style teaching](https://gitlab.gbar.dtu.dk/ELE_group/teaching/teaching_adventures/blob/3ed1b4dec0570792d49b61a87d0ceb15df1980ff/teaching_adventures.pdf)
- version 2.0 of [presentation (for Deans Teaching Seminar, 2021-10) on assessments](https://gitlab.gbar.dtu.dk/ELE_group/teaching/teaching_adventures/-/blob/2021_10_deans_teaching_seminar_exam/teaching_adventures.pdf)
- version 3.0 of [presentation (for DTU Learn superuser meeting 2022-04) on webpage inside Learn - before evaluation](https://gitlab.gbar.dtu.dk/ELE_group/teaching/teaching_adventures/-/blob/2022_04_DTULearn_superbruger_webpage/teaching_adventures.pdf)
- version 4.0 of [presentation (DTU Electro staff meeting, 2022-04) for joining hands-on teaching](https://gitlab.gbar.dtu.dk/ELE_group/teaching/teaching_adventures/-/blob/2022_04_electro_invitation_EK2/teaching_adventures.pdf)
- version 5.0 of [presentation (DTU Electro staff meeting, 2022-05) on webpage inside Learn with evaluation](https://gitlab.gbar.dtu.dk/ELE_group/teaching/teaching_adventures/-/blob/2022_05_electro_webpage/teaching_adventures.pdf)
- version 6.0 of [presentation (meeting with Dean, 2023-06) on webpage version 2 inside Learn with evaluation](https://gitlab.gbar.dtu.dk/ELE_group/teaching/teaching_adventures/-/blob/2023_06_meeting_dean_learning_management_system/teaching_adventures.pdf)
- version 7.0 ov [Reflection on Teaching Adventures for development of teaching and learning award 2023-07-31](https://gitlab.gbar.dtu.dk/ELE_group/teaching/teaching_adventures/-/blob/2023_07_award_development_teaching_n_learning/teaching_adventures.pdf)

## contents

This repository contains the presentation from Arnold Knott about teaching adventures with
- flipped classroom
- videos
- quizzes
- online group exercises
- flexible tool usage

## sources

compile with
`latexmk -pv -xelatex teaching_adventures`

sources are based on template from
[dtutemplates from DTU LaTeX group](https://gitlab.gbar.dtu.dk/latex/dtutemplates)
