source code and presentation in this repository:
https://gitlab.gbar.dtu.dk/ELE_group/teaching/teaching_adventures.git
(login as LDAP user with your DTU credentials)

the presentation version in the faculty meeting on 2021-08-27 was this commit (tagged v1.0):
https://gitlab.gbar.dtu.dk/ELE_group/teaching/teaching_adventures/blob/47107e187c5367a41513cfc8eaf4a615c26506ef/teaching_adventures.pdf
