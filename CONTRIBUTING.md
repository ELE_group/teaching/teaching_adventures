**Thanks for your interest in contributing to this presentation!**

Originally I intended to be the sole contributor to this work, but I encourage you to enhance this presentation.
Please feel free to make a *pull request*.

In case of any questions ping me on [Discord](https://discordapp.com/users/418453367761928194).

Once in a while, I might also read my [email](mailto:akn@elektro.dtu.dk).
